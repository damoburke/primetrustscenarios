package com.t0.pt

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PtScenariosApplication

fun main(args: Array<String>) {
    runApplication<PtScenariosApplication>(*args)
}