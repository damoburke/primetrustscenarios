package com.t0.pt.rest

import com.t0.pt.Scenarios
import org.springframework.web.bind.annotation.*

@RestController
class QuoteHandler(val scenarios: Scenarios) {

    @GetMapping("/pt/bs/all")
    fun buyAndSellAll() {
        scenarios.buyAndSellAll()
    }

    // Failing 5-Feb
    @GetMapping("/pt/b/all")
    fun buyAll() {
        scenarios.buyAll()
    }

    @GetMapping("/pt/bs/{asset}/{times}")
    fun buy(@PathVariable("asset") asset: String, @PathVariable("times") times: Int) {
        scenarios.buyAndSell(asset, times)
    }
}