package com.t0.pt.dto

data class PtRelationships(
    val asset: PtAsset? = null
)