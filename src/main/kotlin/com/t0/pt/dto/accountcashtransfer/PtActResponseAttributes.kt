package com.t0.pt.dto.accountcashtransfer

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class PtActResponseAttributes(
    @JsonProperty("amount")
    var baseAmount: String = "",
    var status: String? = null,
    @JsonProperty("created-at")
    var createdAt: Date? = null,
    @JsonProperty("updated-at")
    var executedAt: Any? = null

)