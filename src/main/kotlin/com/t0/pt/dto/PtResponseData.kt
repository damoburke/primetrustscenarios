package com.t0.pt.dto

data class PtResponseData(
    val type: String? = null,
    val id: String? = null,
    val attributes: PtResponseAttributes? = null,
    val links: PtLinks? = null,
    val relationships: PtRelationships? = null
)