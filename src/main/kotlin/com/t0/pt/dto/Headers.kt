package com.t0.pt.dto

class Headers {


    companion object {

        const val TOKEN =
            "eyJhbGciOiJIUzI1NiJ9.eyJhdXRoX3NlY3JldCI6ImRmNmQ0MjA0LTcwNjQtNGZjOS1hZTYzLTAxYmM5MjNhNDA3ZSIsInVzZXJfZ3JvdXBzIjpbXSwibm93IjoxNjM3OTI3NDA3LCJleHAiOjE2Mzg1MzIyMDd9.n8Q-0WrToyIMZDMd4hdNthOqjWuckeI8rGX-QxUe0YM"

        const val JWT =
            "Authorization=Bearer $TOKEN"

        const val HOST = "Host=sandbox.primetrust.com"
        const val USER_AGENT = "User-Agent=damo-test"
    }
}