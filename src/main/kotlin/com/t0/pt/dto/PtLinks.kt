package com.t0.pt.dto

data class PtLinks(
    val self: String? = null,
    val related: String? = null
)