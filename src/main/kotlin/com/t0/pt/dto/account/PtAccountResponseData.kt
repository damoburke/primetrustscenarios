package com.t0.pt.dto.account

import com.t0.pt.dto.PtLinks
import com.t0.pt.dto.PtRelationships

data class PtAccountResponseData(
    val type: String? = null,
    val id: String,
    val links: PtLinks? = null,
    val relationships: PtRelationships? = null
)