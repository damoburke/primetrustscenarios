package com.t0.pt.dto

data class PtRequestData(
    val type: String,
    val attributes: PtRequestAttributes
)