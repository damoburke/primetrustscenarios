package com.t0.pt.dto

data class PtAsset(
    val links: PtLinks? = null
)