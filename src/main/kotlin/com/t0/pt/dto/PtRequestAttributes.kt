package com.t0.pt.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class PtRequestAttributes(

    @JsonProperty("total-amount")
    val totalAmount: String,

    @JsonProperty("asset-id")
    val assetId: String,

    @JsonProperty("account-id")
    val accountId: String,
    val hot: Boolean = false,

    @JsonProperty("transaction-type")
    val type: String = "buy",

    @JsonProperty("trade-desk-id")
    val tradeDeskId: String

)