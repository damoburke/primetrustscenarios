package com.t0.pt.service

class RequestStubs {

    companion object {


        const val SANDBOX_FUNDS = "{\n" +
                "  \"data\": {\n" +
                "    \"type\": \"accounts\",\n" +
                "    \"attributes\": {\n" +
                "      \"amount\": 100000\n" +
                "    }\n" +
                "  }\n" +
                "}"

        const val ACCOUNT_CREATE = "{\n" +
                "\t\"data\" : {\n" +
                "\t\t\"type\" : \"account\",\n" +
                "\t\t\"attributes\" : {\n" +
                "\t\t\t\"account-type\" : \"custodial\",\n" +
                "\t\t\t\"name\" : \"Damien Burke 2 new JWT\",\n" +
                "\t\t\t\"authorized-signature\" : \"Damien Burke 2\",\n" +
                "\t\t\t\"owner\" : {\n" +
                "\t\t\t\t\"contact-type\" : \"natural_person\",\n" +
                "\t\t\t\t\"name\" : \"Damien Burke 2 new JWT\",\n" +
                "\t\t\t\t\"email\" : \"DamienBurke@gmail.com\",\n" +
                "\t\t\t\t\"date-of-birth\" : \"1-1-1980\",\n" +
                "\t\t\t\t\"tax-id-number\" : \"123456789\",\n" +
                "\t\t\t\t\"tax-country\" : \"US\",\n" +
                "\t\t\t\t\"primary-phone-number\" : {\n" +
                "\t\t\t\t\t\"country\" : \"US\",\n" +
                "\t\t\t\t\t\"number\" : \"123456789\",\n" +
                "\t\t\t\t\t\"sms\" : false\n" +
                "\t\t\t\t},\n" +
                "\t\t\t\t\"primary-address\" : {\n" +
                "\t\t\t\t\t\"street-1\" : \"add 1\",\n" +
                "\t\t\t\t\t\"street-2\" : \"add 2\",\n" +
                "\t\t\t\t\t\"postal-code\" : \"12345\",\n" +
                "\t\t\t\t\t\"city\" : \"city\",\n" +
                "\t\t\t\t\t\"region\" : \"NY\",\n" +
                "\t\t\t\t\t\"country\" : \"US\"\n" +
                "\t\t\t\t}\n" +
                "\t\t\t},\n" +
                "\t\t\t\"webhook-config\": {\n" +
                "                \"contact-email\": \"dburke@tzero.com\",\n" +
                "                \"url\": \"https://us-central1-tw-services-local-v2.cloudfunctions.net/ptwebhook\",\n" +
                "                \"enabled\": true\n" +
                "            }\n" +
                "\t\t}\n" +
                "\t}\n" +
                "}"


    }

}
