package com.t0.pt.service


import com.t0.pt.dto.Headers
import com.t0.pt.dto.PtRequest
import com.t0.pt.dto.PtResponse
import com.t0.pt.dto.PtTradeResponse
import feign.Body
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

@FeignClient(
    name = "pt",
    url = "https://sandbox.primetrust.com",
    configuration = [PtConfig::class]
)
interface PtClient {

    @PostMapping(
        value = ["v2/quotes"],
        headers = [Headers.HOST, Headers.USER_AGENT, Headers.JWT, "Connection: keep-alive", "Accept: */*"]
    )
    fun createQuote(request: PtRequest?): PtResponse


    @Body("")
    @PostMapping(
        value = ["/v2/quotes/{quoteId}/execute"],
        headers = [Headers.HOST, Headers.USER_AGENT, Headers.JWT, "Connection: keep-alive", "Accept: */*"]
    )
    fun executeQuote(@PathVariable("quoteId") quoteId: String): PtTradeResponse


    @PostMapping(
        value = ["/v2/accounts?include=contacts"],
        headers = [Headers.HOST, Headers.USER_AGENT, Headers.JWT]
    )
    fun createAccount(@RequestBody json: String): String

    @Body("")
    @PostMapping(
        value = ["/v2/accounts/{accountId}/sandbox/open"],
        headers = [Headers.HOST, Headers.USER_AGENT, Headers.JWT]
    )
    fun openAccount(@PathVariable("accountId") accountId: String)

    @PostMapping(
        value = ["/v2/accounts/{accountId}/sandbox/fund"],
        headers = [Headers.HOST, Headers.USER_AGENT, Headers.JWT]
    )
    fun fundAccount(@PathVariable("accountId") accountId: String, @RequestBody json: String)


}



