package com.t0.pt

import com.t0.pt.service.PtService
import org.springframework.stereotype.Service

@Service
class Scenarios(val ptService: PtService) {

    val assets = mapOf(
        "btc" to "798debbc-ec84-43ea-8096-13e2ebcf4749",
        "ltc" to "27552c2e-7ddb-4144-81e3-23f87c94da3f",
        "eth" to "e63b0367-c47b-49be-987a-f14036b230cd"
    )


    fun buyAndSellAll() {

        val accountId = ptService.configureNewAccount()

        assets.values.forEach { asset ->
            buy(asset = asset, accountId = accountId)
            Thread.sleep(10000)
            sell(asset = asset, accountId = accountId)
        }
    }

    fun buyAll() {

        val accountId = ptService.configureNewAccount()

        assets.values.forEach { asset ->
            buy(asset = asset, accountId = accountId)
        }
    }

    fun buyAndSell(asset: String, n: Int = 1) {

        val accountId = ptService.configureNewAccount()

        val asset = assets.get(asset)!!

        repeat(n) {
            buy(asset = asset, accountId = accountId)
            sell(asset = asset, accountId = accountId)
        }

    }

    private fun buy(accountId: String, asset: String) {
        val buyQuoteId = ptService.createQuote(accId = accountId, asset = asset, type = "buy", amount = "20")

        try {
            ptService.executeQuote(buyQuoteId, asset, "buy")
        } catch (e: Exception) {
            println("error first time buying $e")
            ptService.executeQuote(buyQuoteId, asset, "buy")
        }
    }

    private fun sell(accountId: String, asset: String) {
        val sellQuoteId = ptService.createQuote(accId = accountId, asset = asset, type = "sell", amount = "10")

        try {
            ptService.executeQuote(sellQuoteId, asset, "sell")
        } catch (e: Exception) {
            println("error first time selling $e")
            ptService.executeQuote(sellQuoteId, asset, "sell")
        }

    }

}

